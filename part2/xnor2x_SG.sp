*************************
*  XNOR_2X SG Mode hspice
*************************

.include './hp7nfet.pm'
.include './hp7pfet.pm'

*define parameters
.param Vdd=0.7
.param Gnd=0
.param fin_height=18n
.param fin_width=7n
.param lg=11n
.param number_pfin_fin = 5
.param number_nfin_fin = 5
.GLOBAL gnd! vdd!

Vvdd vdd! 0 'Vdd'
Vgnd gnd! 0 'Gnd'

*add transistors
*pfet is for the finfet nfet
.SUBCKT inv vi vo
mn1 vo vi gnd! vi nfet L=lg NFIN=1
mp1 vo vi vdd! vi pfet L=lg NFIN=1
.ENDS
XAINV A A_INV INV
XBINV B B_INV INV

mn4 Z A_INV net4 A_INV nfet L=lg NFIN=number_nfin_fin
mn3 net4 B gnd! B nfet L=lg NFIN=number_nfin_fin
mn2 Z A net3 A nfet L=lg NFIN=number_nfin_fin
mn1 net3 B_INV gnd! B_INV nfet L=lg NFIN=number_nfin_fin

mp4 Z A_INV net2 A_INV pfet L=lg NFIN=number_pfin_fin
mp3 net2 B_INV vdd! B_INV pfet L=lg NFIN=number_pfin_fin
mp2 Z B net1 B pfet L=lg NFIN=number_pfin_fin
mp1 net1 A vdd! A pfet L=lg NFIN=number_pfin_fin

*cap at the output of the transistors
C0 Z gnd! 2e-15F

*input Signal
VINa A 0 0 pulse 0 0.7 0 10p 10p 0.4n 1n
*VINb B 0 0 pulse 0 0.7 0 10p 10p 0.9n 2n
VINb B 0 0 pwl 0 0 20n 0

*do transient analysis
	*syntax: .TRAN tiner tstop START=stval
	*tiner - time step
	*tstop - final time
	*stval - initial time (default 0)
.tran 10p 10n

*print the V(Z) to waveform file *.tr0
.print V(Z)
.print V(A)

*simulation options (you can modify this. Post is needed for .tran analysis)
.OPTION Post measout

*measurement
.MEAS t_rise TRIG v(Z) VAL='Vdd*0.2' RISE=1 TARG v(Z) VAL='Vdd*0.8' RISE=1
.MEAS t_fall TRIG v(Z) VAL='Vdd*0.8' FALL=1 TARG v(Z) VAL='Vdd*0.2' FALL=1
.MEAS TRAN t_fall_delay TRIG v(A) VAL='Vdd*0.5' RISE=1 CROSS=1 TARG v(Z) VAL='Vdd*0.5' CROSS=1 FALL=1
.MEAS TRAN t_rise_delay TRIG v(A) VAL='Vdd*0.5' FALL=1 CROSS=1 TARG v(Z) VAL='Vdd*0.5' CROSS=1 RISE=1
.end
