#!/usr/bin/python
# -*- coding: utf-8 -*-

"""Lab3 assignment submission.
"""

import sys
import re
import os
import math
import subprocess
import collections

__author__ = 'Sparsh Gupta'
__version__ = '1.0.0'
__maintainer__ = 'Sparsh Gupta'
__email__ = 'sparshgu@usc.edu'

finRegex1 = re.compile(r'.param number_pfin_fin = (\d+)')
finRegex2 = re.compile(r'.param number_nfin_fin = (\d+)')
SweepResults = collections.namedtuple('SweepResults',
        't_rise_delay t_fall_delay t_rise t_fall')
SweepParam = collections.namedtuple('SweepParam', 'FPin FNin')
dcSweepDict = collections.OrderedDict()


def readHspiceOutputFile(fileOutputPath, f_p_in, f_n_in):
    if os.path.isfile(fileOutputPath):
        f = open(fileOutputPath, 'r')
        try:
            try:
                mtFileLines = f.readlines()
                if mtFileLines[4] is not None:
                    numValues = [x.strip() for x in
                                 mtFileLines[4].split(' ') if x.strip()]
                    if len(numValues) == 4:
                        key = SweepParam(FPin=f_p_in, FNin=f_n_in)
                        try:
                            value = \
                                SweepResults(t_rise_delay=float(numValues[2]),
                                    t_fall_delay=float(numValues[3]),
                                    t_rise=float(numValues[0]),
                                    t_fall=float(numValues[1]))
                        except:
                            value = \
                                SweepResults(t_rise_delay=float(-1),
                                    t_fall_delay=float(-1),
                                    t_rise=float(-1),
                                    t_fall=float(-1))
                        dcSweepDict[key] = value
                    else:
                        raise Exception('%s doesn\'t contain 4 values for SweepParam'
                                 % fileOutputPath)
                else:
                    raise Exception('%s doesn\'t contain values for SweepParam. Not enough lines'
                                     % fileOutputPath)
            except:
                raise Exception('Error in reading the %s.Exception: %s'
                                % (fileOutputPath, sys.exc_info()[0]))
        finally:
            f.close()
            try:
                os.remove(fileOutputPath)
                os.remove(os.path.splitext(fileOutputPath)[0] + '.st0')
                os.remove(os.path.splitext(fileOutputPath)[0] + '.tr0')
                os.remove(os.path.splitext(fileOutputPath)[0] + '.ic0')
            except OSError:
                pass
    else:
        raise Exception('Error in reading the %s. File didn\'t  get generated.'
                         % fileOutputPath)


def printHspiceFile(content, fileOutputPath):
    f = open(fileOutputPath, 'w')
    try:
        try:
            print >> f, content
        except:
            raise Exception('Error in writing the %s.Exception: %s'
                            % (fileOutputPath, sys.exc_info()[0]))
    finally:
        f.close()


def executeHspiceFile(fileInputPath, f_p_in, f_n_in):
    try:
        res = subprocess.Popen(['hspice', fileInputPath],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        (output, error) = res.communicate()
        if output:
            readHspiceOutputFile(os.path.splitext(fileInputPath)[0]
                                 + '.mt0', f_p_in, f_n_in)
    except OSError, e:

        # if error:
        #    raise Exception('Error in executing.ReturnCode> %s ,ErrorMessage> %s'%(res.returncode,error.strip()))

        raise Exception('Error in executing.ReturnCode> %s ,ErrorMessage> %s'
                         % (e.args[0], e.args[1]))
    except:
        raise Exception('Error in executing.ErrorType> %s'
                        % sys.exc_info()[0])
    finally:
        try:
            pass
            os.remove(fileInputPath)
        except OSError:
            pass


def createHSpiceFilesFromTemplate(fileInputPath, startVal, endVal):
    f = open(fileInputPath, 'r')
    try:
        try:
            inputFileLines = f.read()
            matchObj1 = finRegex2.search(inputFileLines, re.M | re.I)
            matchObj2 = finRegex1.search(inputFileLines, re.M | re.I)
            if None != matchObj1 and None != matchObj2:
                for i in range(startVal, endVal):
                    for j in range(startVal, endVal):
                        resultString = \
                            finRegex1.sub(r'.param number_pfin_fin ='
                                + str(i), inputFileLines, re.M | re.I)
                        resultString = \
                            finRegex2.sub(r'.param number_nfin_fin ='
                                + str(j), resultString, re.M | re.I)
                        outputFile = os.path.splitext(fileInputPath)[0] \
                            + '_' + str(i) + '_' + str(j) + '.sp'
                        printHspiceFile(resultString, outputFile)
                        executeHspiceFile(outputFile, i, j)
                printDCSweep(os.path.splitext(fileInputPath)[0])
            else:
                print 'No match. File template incorrect.'
        except:
            print 'Error reading the Inputfile .Exception: %s' \
                % sys.exc_info()[0]
    finally:
        f.close()


def printDCSweep(outputFileTemplate):
    f = open(outputFileTemplate + '_report.txt', 'w')
    try:
        try:
            print >> f, \
                '%s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%s' \
                % (
                'FPin',
                'FNin',
                't_rise_delay',
                't_fall_delay',
                '((rise-fall)/rise)%',
                't_rise',
                't_fall',
                )
            for (key, value) in dcSweepDict.iteritems():
                print >> f, \
                    '%s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%s' \
                    % (
                    key.FPin,
                    key.FNin,
                    value.t_rise_delay,
                    value.t_fall_delay,
                    100 * ((value.t_rise_delay - value.t_fall_delay)
                           / value.t_rise_delay),
                    value.t_rise,
                    value.t_fall,
                    )
        except:
            raise Exception('Error in writing the %s.Exception: %s'
                            % (fileOutputPath, sys.exc_info()[0]))
    finally:
        f.close()


def main(arguments):
    if len(arguments) != 3:
        print 'usage: sweepFin.py -t=<InputFileTemplate> -s=<startVal> -e=<endVal>'
        sys.exit(1)
    if not '-t=' in arguments[0]:
        print 'usage: sweepFin.py -t=<InputFileTemplate> -s=<startVal> -e=<endVal>'
        sys.exit(1)
    if not '-s=' in arguments[1]:
        print 'usage: sweepFin.py -t=<InputFileTemplate> -s=<startVal> -e=<endVal>'
        sys.exit(1)
    if not '-e=' in arguments[2]:
        print 'usage: sweepFin.py -t=<InputFileTemplate> -s=<startVal> -e=<endVal>'
        sys.exit(1)
    fileInputPath = (arguments[0])[3:]
    startVal = (arguments[1])[3:]
    endVal = (arguments[2])[3:]
    if int(startVal) > int(endVal):
        print 'startval should be less than endVal'
        sys.exit(1)
    if not os.path.isfile(fileInputPath):
        print 'usage: sweepFin.py -t=<InputFile> -s=<startVal> -e=<endVal>'
        print "InputFile doesn't exist"
        sys.exit(1)
    createHSpiceFilesFromTemplate(fileInputPath, int(startVal),
                                  int(endVal))


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
