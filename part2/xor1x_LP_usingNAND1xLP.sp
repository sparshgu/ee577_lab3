*************************
*  XOR_1X using NAND LP Mode hspice
*************************

.include './hp7nfet.pm'
.include './hp7pfet.pm'

*define parameters
.param Vdd=0.7
.param Gnd=0
.param fin_height=18n
.param fin_width=7n
.param lg=11n
.param number_fin = 1
.GLOBAL gnd! vdd!

Vvdd vdd! 0 'Vdd'
Vgnd gnd! 0 'Gnd'

*add transistors
*pfet is for the finfet nfet
.SUBCKT nand1x_LP vA vB vo
mn2 vo vA net1 gnd! nfet L=lg NFIN=3
mn1 net1 vB gnd! gnd! nfet L=lg NFIN=3
mp2 vo vB vdd! vdd! pfet L=lg NFIN=2
mp1 vo vA vdd! vdd! pfet L=lg NFIN=2
.ENDS

Xnand1x_LP0 A B net1 nand1x_LP
Xnand1x_LP1 A net1 net2 nand1x_LP
Xnand1x_LP2 net1 B net3 nand1x_LP
Xnand1x_LP3 net2 net3 Z nand1x_LP



*cap at the output of the transistors
C0 Z gnd! 2e-15F

*input Signal
VINa A 0 0 pulse 0 0.7 0 10p 10p 0.4n 1n
VINb B 0 0 pulse 0 0.7 0 10p 10p 0.9n 2n

*do transient analysis
	*syntax: .TRAN tiner tstop START=stval
	*tiner - time step
	*tstop - final time
	*stval - initial time (default 0)
.tran 10p 10n

*print the V(Z) to waveform file *.tr0
.print V(Z)
.print V(A)

*simulation options (you can modify this. Post is needed for .tran analysis)
.OPTION Post measout

*measurement
.MEAS t_rise TRIG v(Z) VAL='Vdd*0.2' RISE=1 TARG v(Z) VAL='Vdd*0.8' RISE=1
.MEAS t_fall TRIG v(Z) VAL='Vdd*0.8' FALL=1 TARG v(Z) VAL='Vdd*0.2' FALL=1
.MEAS TRAN t_fall_delay TRIG v(A) VAL='Vdd*0.5' RISE=1 CROSS=1 TARG v(Z) VAL='Vdd*0.5' CROSS=1 FALL=1
.MEAS TRAN t_rise_delay TRIG v(A) VAL='Vdd*0.5' FALL=1 CROSS=1 TARG v(Z) VAL='Vdd*0.5' CROSS=1 RISE=1
.end
