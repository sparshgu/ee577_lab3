*************************
*  XOR_1X IG:LP Mode hspice
*************************

.include './hp7nfet.pm'
.include './hp7pfet.pm'

*define parameters
.param Vdd=0.7
.param Gnd=0
.param fin_height=18n
.param fin_width=7n
.param lg=11n
.param number_pfin_fin = 2
.param number_nfin_fin = 2
.param vhi=1.2
.param vlo=-0.2
.GLOBAL gnd! vdd!

Vvdd vdd! 0 'Vdd'
Vgnd gnd! 0 'Gnd'

Vn Vn 0 'vlo'
Vp Vp 0 'vhi'

*add transistors
*pfet is for the finfet nfet
.SUBCKT inv vi vo
mn1 vo vi gnd! vi nfet L=lg NFIN=1
mp1 vo vi vdd! vi pfet L=lg NFIN=1
.ENDS
XAINV A A_INV INV
XBINV B B_INV INV

mn4 Z A_INV net4 Vn nfet L=lg NFIN=number_nfin_fin
mn3 net4 B_INV gnd! Vn nfet L=lg NFIN=number_nfin_fin
mn2 Z A net3 Vn nfet L=lg NFIN=number_nfin_fin
mn1 net3 B gnd! Vn nfet L=lg NFIN=number_nfin_fin

mp2 Z A vdd! B_INV pfet L=lg NFIN=number_pfin_fin
mp1 Z A_INV vdd! B pfet L=lg NFIN=number_pfin_fin

*cap at the output of the transistors
C0 Z gnd! 2e-15F

*input Signal
VINa A 0 0 pulse 0 0.7 0 10p 10p 0.4n 1n
*VINb B 0 0 pulse 0 0.7 0 10p 10p 0.9n 2n
VINb B 0 0 pwl 0 0.7 20n 0.7

*do transient analysis
	*syntax: .TRAN tiner tstop START=stval
	*tiner - time step
	*tstop - final time
	*stval - initial time (default 0)
.tran 10p 10n

*print the V(Z) to waveform file *.tr0
.print V(Z)
.print V(A)

*simulation options (you can modify this. Post is needed for .tran analysis)
.OPTION Post measout

*measurement
.MEAS t_rise TRIG v(Z) VAL='((Vdd-0.522)*0.2)+0.522' RISE=1 TARG v(Z) VAL='((Vdd-0.522)*0.8)+0.522' RISE=1
.MEAS t_fall TRIG v(Z) VAL='((Vdd-0.522)*0.8)+0.522' FALL=1 TARG v(Z) VAL='((Vdd-0.522)*0.2)+0.522' FALL=1
.MEAS TRAN t_fall_delay TRIG v(A) VAL='Vdd*0.5' RISE=1 CROSS=1 TARG v(Z) VAL='((Vdd-0.522)*0.5)+0.522' CROSS=1 FALL=1
.MEAS TRAN t_rise_delay TRIG v(A) VAL='Vdd*0.5' FALL=1 CROSS=1 TARG v(Z) VAL='((Vdd-0.522)*0.5)+0.522' CROSS=1 RISE=1
*Power measurement
.MEAS TRAN RTL_F TRIG AT=0 TARG v(Z) VAL=0.67 FALL=1
.MEAS TRAN avg_current_F AVG I(C0) from 0 to 'RTL_F'
.MEAS TRAN avg_power_F AVG p(C0)
.MEAS TRAN Icct_F INTEG I(C0) from 0 to 'RTL_F'
.MEAS E_Switch_F Param='-Icct_F*vdd'
.MEAS TRAN leakagePower_f AVG p(mp1) from 0 to 'RTL_F'

.MEAS TRAN RTL_R TRIG AT=0 TARG v(Z) VAL=0.6 RISE=1
.MEAS TRAN avg_current_R AVG I(C0) from 0 to 'RTL_R'
.MEAS TRAN avg_power_R AVG p(C0)
.MEAS TRAN Icct_R INTEG I(C0) from 0 to 'RTL_R'
.MEAS E_Switch_R Param='-Icct_R*vdd'
.MEAS TRAN leakagePower_r AVG p(mn2) from 0 to 'RTL_R'
.end
