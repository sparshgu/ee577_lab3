#!/usr/bin/env python

"""Lab3 assignment submission.
"""
import sys
import re
import os
import math
import subprocess
import collections

__author__ = "Sparsh Gupta"
__version__ = "1.0.0"
__maintainer__ = "Sparsh Gupta"
__email__ = "sparshgu@usc.edu"

finRegex= re.compile(r'.param number_fin = (1)')
capRegex = re.compile(r'.param LoadCap = ([0-9]*\.?[0-9]+)f')
capValues = [math.pow(5,round(x*0.1,2)) for x in range(1, 11)]
capValues.insert(0,0.01)
transitionEndTimeRegex = re.compile(r'.tran 0.1u (\d+)u')
SweepResults = collections.namedtuple('SweepResults', 'rtl Iavg Pavg')
SweepParam = collections.namedtuple('SweepParam', 'Fin C')
dcSweepDict = collections.OrderedDict()
def readHspiceOutputFile(fileOutputPath,f_in,c_val):
    if os.path.isfile(fileOutputPath):
        f = open(fileOutputPath,'r')
        try:
            try:
                mtFileLines = f.readlines()
                if (mtFileLines[4] is not None):
                    numValues = [x.strip() for x in mtFileLines[4].split(' ') if (x.strip())]
                    if (len(numValues)==4):
                        key = SweepParam(Fin=f_in,C= c_val)
                        value = SweepResults(rtl=float(numValues[0]), Iavg=float(numValues[1]), Pavg=float(numValues[2]))
                        dcSweepDict[key]=value
                    else:
                        raise Exception('%s doesn\'t contain 4 values for SweepParam' %(fileOutputPath))
                else:
                    raise Exception('%s doesn\'t contain values for SweepParam. Not enough lines' %(fileOutputPath))
            except:
                raise Exception('Error in reading the %s.Exception: %s' %(fileOutputPath, sys.exc_info()[0]))
        finally:
            f.close()
            try:
                os.remove(fileOutputPath)
                os.remove(os.path.splitext(fileOutputPath)[0]+".st0")
                os.remove(os.path.splitext(fileOutputPath)[0]+".tr0")
                os.remove(os.path.splitext(fileOutputPath)[0]+".ic0")
            except OSError:
                pass
    else:
        raise Exception('Error in reading the %s. File didn\'t  get generated.' %(fileOutputPath))

def printHspiceFile(content,fileOutputPath):
    f = open(fileOutputPath,'w')
    try:
        try:
            print >> f,content
        except:
            raise Exception('Error in writing the %s.Exception: %s' %(fileOutputPath, sys.exc_info()[0]))
    finally:
        f.close()

def executeHspiceFile(fileInputPath,f_in,c_val):
    try:
        res = subprocess.Popen(['hspice',fileInputPath ],
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)
        (output, error) = res.communicate()
        if output:
            readHspiceOutputFile(os.path.splitext(fileInputPath)[0]+".mt0",f_in,c_val)
        #if error:
        #    raise Exception('Error in executing.ReturnCode> %s ,ErrorMessage> %s'%(res.returncode,error.strip()))
    except OSError, e:
        raise Exception('Error in executing.ReturnCode> %s ,ErrorMessage> %s'%(e.args[0],e.args[1]))
    except:
        raise Exception('Error in executing.ErrorType> %s'%(sys.exc_info()[0]))
    finally:
        try:
            pass
	    #os.remove(fileInputPath)
        except OSError:
            pass

def getFinalTransitionMultiplierTimeValue(i):
    if (0<=i<=1):
        return 1
    elif (1<=i<=2):
        return 2
    elif (3<=i<=4):
        return 3
    elif (5<=i<=6):
        return 4
    elif (7<=i<=8):
        return 5
    elif (9<=i<=10):
        return 6
    else:
        return 8


def createHSpiceFilesFromTemplate(fileInputPath):
    f = open(fileInputPath, 'r')
    try:
        try:
            inputFileLines = f.read()
            matchObj1 = capRegex.search(inputFileLines, re.M|re.I)
            matchObj2 = finRegex.search(inputFileLines, re.M|re.I)
            if (None!=matchObj1 and None != matchObj2):
                for i in range(1,17):
                    resultString = finRegex.sub(r'.param number_fin = '+str(i),inputFileLines,re.M|re.I)
                    index = 0
                    for capValue in capValues:
                        resultString=capRegex.sub(r'.param LoadCap = %sf'%str(capValue),resultString,re.M|re.I)
                        resultString=transitionEndTimeRegex.sub(r'.tran 0.1u %su'%str(20*getFinalTransitionMultiplierTimeValue(index)),resultString,re.M|re.I)
                        index+=1
                        try:
                            #print str(i) + ",capValue= "+str(capValue)
                            outputFile = 'dram_'+str(i)+'_'+str(capValue)+'.sp'
                            printHspiceFile(resultString,outputFile)
                            executeHspiceFile(outputFile,i,capValue)
                        except Exception,e:
                            print ("Error in generating/executing hspice file for F_in=%s and CapValue=%s. Continuing to next iteration." %(str(i),str(capValue)))
                            print e.message
                            continue
                printDCSweep()
            else:
                print "No match. File template incorrect."
        except:
            print('Error reading the Inputfile .Exception: %s' % sys.exc_info()[0])
    finally:
        f.close()

def printDCSweep():
    f = open('dram.txt','w')

    try:
        try:
            print >> f,'%s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%s'%('Fin','C','R.T.L','Iavg','Pavg')
            for key, value in dcSweepDict.iteritems():
                print >> f,("%s\t\t%-10s\t\t%-10s\t\t%-10s\t\t%s"%(key.Fin,key.C,value.rtl,value.Iavg,value.Pavg))
        except:
            raise Exception('Error in writing the %s.Exception: %s' %(fileOutputPath, sys.exc_info()[0]))
    finally:
        f.close()



def main(arguments):
    if (len(arguments) != 1):
        print("usage: script.py -t=<InputFileTemplate>")
        sys.exit(1)
    if (not '-t=' in arguments[0]):
        print("usage: script.py -t=<InputFileTemplate>")
        sys.exit(1)
    fileInputPath = arguments[0][3:]
    if (not os.path.isfile(fileInputPath)):
        print("usage: script.py -t=<InputFile>")
        print("InputFile doesn't exist")
        sys.exit(1)
    createHSpiceFilesFromTemplate(fileInputPath)
if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
