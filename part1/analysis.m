dramParam = importdata('dram.txt','\t',1);
Fin = dramParam.data(:, 1);
C = dramParam.data(:, 2);
RTL = dramParam.data(:, 3);
Iavg = dramParam.data(:, 4);
Pavg = dramParam.data(:, 5);
CplusFin = (C+ Fin);
uniqueC = unique(C)';
uniqueFin = unique(Fin)';
[CMesh,FMesh] = meshgrid(uniqueC,uniqueFin);
CMeshPlusFMesh = CMesh + FMesh;
uniqueCSize = size(uniqueC);
uniqueFinSize = size(uniqueFin);
uniqueCplusFinSize = size(CplusFin);
RTLZeros = zeros(uniqueFinSize(2),uniqueCSize(2));
IavgZeros = zeros(uniqueFinSize(2),uniqueCSize(2));
PavgZeros = zeros(uniqueFinSize(2),uniqueCSize(2));
for i = 1:uniqueCSize(2)
    for j = 1:uniqueFinSize(2)
        for k = 1:uniqueCplusFinSize(1)
            if (CMeshPlusFMesh(j,i)==CplusFin(k,1))
                tmp = RTL(k);
            end
        end
        RTLZeros(j,i) = tmp;
    end
end
for i = 1:uniqueCSize(2)
    for j = 1:uniqueFinSize(2)
        for k = 1:uniqueCplusFinSize(1)
            if (CMeshPlusFMesh(j,i)==CplusFin(k,1))
                tmp = Iavg(k);
            end
        end
        IavgZeros(j,i) = tmp;
    end
end
for i = 1:uniqueCSize(2)
    for j = 1:uniqueFinSize(2)
        for k = 1:uniqueCplusFinSize(1)
            if (CMeshPlusFMesh(j,i)==CplusFin(k,1))
                tmp = Pavg(k);
            end
        end
        PavgZeros(j,i) = tmp;
    end
end
figure(1);
m=surf(CMesh,FMesh,RTLZeros);
xlabel('Cload'); ylabel('Number of Fin'); zlabel("R.T.L");
title('R.T.L vs (Cload,Number of Fin)')
axis tight;
colorbar;
figure(2);
surf(CMesh,FMesh,IavgZeros);
xlabel("Cload"); ylabel("Number of Fin"); zlabel("Iavg");
title("Avg Current vs (Cload,Number of Fin)");
axis tight;
colorbar;
figure(3);
surf(CMesh,FMesh,PavgZeros);
xlabel("Cload"); ylabel("Number of Fin"); zlabel("Pavg");
title("Avg Power vs (Cload,Number of Fin)");
axis tight;
colorbar;